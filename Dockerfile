FROM registry.gitlab.com/etke.cc/base AS builder

WORKDIR /miounne
COPY . .
RUN make build

FROM alpine:latest

ENV MIOUNNE_MATRIX_DATABASE_URL /data/miounne.db

RUN apk --no-cache add ca-certificates tzdata olm && update-ca-certificates && \
    adduser -D -g '' miounne && \
    mkdir /data && chown -R miounne /data

COPY --from=builder /miounne/miounne /opt/miounne/miounne

WORKDIR /opt/miounne
USER miounne

ENTRYPOINT ["/opt/miounne/miounne"]

