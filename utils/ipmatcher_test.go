package utils

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/suite"
)

type IPMatcherSuite struct {
	suite.Suite
}

func (suite *IPMatcherSuite) SetupTest() {
	suite.T().Helper()
}

func (suite *IPMatcherSuite) TestMatch() {
	matcher := NewIPMatcher(true)
	tests := []struct {
		input    string
		expected string
	}{
		{input: "127.0.0.1,192.168.0.1,1.1.1.1", expected: "1.1.1.1"},
		{input: "127.0.0.1", expected: ""},
		{input: "192.168.0.1", expected: ""},
		{input: "100.64.0.1", expected: ""},
		{input: "1.1.1.1,1.0.0.1", expected: "1.0.0.1"},
	}

	for _, test := range tests {
		request, err := http.NewRequest("GET", "https://example.com", nil)
		request.Header.Add("X-Forwarded-For", test.input)

		ip := matcher.Match(request)

		suite.NoError(err)
		suite.Equal(test.expected, ip)
	}
}

func (suite *IPMatcherSuite) TestMatchHash() {
	var expectedIP string
	var expectedHash uint32 = 87490893
	matcher := NewIPMatcher(false)
	request, _ := http.NewRequest("GET", "https://example.com", nil)
	request.Header.Add("X-Forwarded-For", "1.1.1.1,1.0.0.1")

	ip := matcher.Match(request)
	hash := matcher.MatchHash(request)

	suite.Equal(expectedIP, ip)
	suite.Equal(expectedHash, hash)
}

func (suite *IPMatcherSuite) TestMatch_Disabled() {
	matcher := NewIPMatcher(false)
	request, err := http.NewRequest("GET", "https://example.com", nil)
	request.Header.Add("X-Forwarded-For", "1.1.1.1,123.123.123.123")

	ip := matcher.Match(request)

	suite.NoError(err)
	suite.Empty(ip)
}

func TestIPMatcherSuite(t *testing.T) {
	suite.Run(t, new(IPMatcherSuite))
}
