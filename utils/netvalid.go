package utils

import "net"

// NetworkValidator checks DNS entries
type NetworkValidator interface {
	HasA(host string) bool
	HasCNAME(host string) bool
	HasMX(host string) bool
}

type netValidator struct {
	log *Logger
}

// NewNetworkValidator object
func NewNetworkValidator(logLevel string) NetworkValidator {
	return &netValidator{log: NewLogger("net.", logLevel)}
}

// HasA checks if host has DNS A records
func (v *netValidator) HasA(host string) bool {
	ips, err := net.LookupIP(host)
	if err != nil {
		v.log.Error("cannot get A records of %s: %v", host, err)
		return false
	}
	v.log.Debug("%s A = %v", host, ips)

	return len(ips) > 0
}

// HasCNAME checks if host has DNS CNAME records
func (v *netValidator) HasCNAME(host string) bool {
	cname, err := net.LookupCNAME(host)
	if err != nil {
		v.log.Error("cannot get CNAME record of %s: %v", host, err)
		return false
	}
	v.log.Debug("%s CNAME = %s", host, cname)

	return cname != ""
}

// HasMX checks if host has DNS MX records
func (v *netValidator) HasMX(host string) bool {
	mxs, err := net.LookupMX(host)
	if err != nil {
		v.log.Error("cannot get MX records of %s: %v", host, err)
		return false
	}
	v.log.Debug("%s MX = %v", host, mxs)

	return len(mxs) > 0
}
