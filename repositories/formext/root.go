package formext

// root form extension/parser
type root struct{}

// ExecuteMarkdown root extension/parser
func (ext *root) ExecuteMarkdown(name string, data map[string]string) string {
	fields := sortKeys(data)
	out := "**New " + name + "**"
	if data["email"] != "" {
		out += " by " + data["email"] + "\n\n"
	} else {
		out += "\n\n"
	}

	for _, field := range fields {
		value := data[field]
		if value != "" {
			out += "* " + field + ": " + value + "\n"
		}
	}

	return out
}

// ExecuteHTML root extension/parser
func (ext *root) ExecuteHTML(name string, data map[string]string) string {
	fields := sortKeys(data)
	out := "<b>New " + name + "</b>"
	if data["email"] != "" {
		out += " by " + data["email"] + "<br>"
	} else {
		out += "<br>"
	}

	for _, field := range fields {
		value := data[field]
		if value != "" {
			out += field + ": " + value + "<br>"
		}
	}

	return out
}
