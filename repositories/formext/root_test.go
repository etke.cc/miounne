package formext

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type RootExtensionSuite struct {
	suite.Suite
}

func (suite *RootExtensionSuite) SetupTest() {
	suite.T().Helper()
}

func (suite *RootExtensionSuite) TearDownTest() {
	suite.T().Helper()
}

func (suite *RootExtensionSuite) TestExecuteMarkdown() {
	input := map[string]string{
		"email": "test@test.com",
		"name":  "Test",
		"phone": "+1234567890",
	}
	expected := "**New test** by test@test.com\n\n* email: test@test.com\n* name: Test\n* phone: +1234567890\n"

	actual := NewRoot().ExecuteMarkdown(name, input)

	suite.Equal(expected, actual)
}

func (suite *RootExtensionSuite) TestExecuteMarkdown_WithoutEmail() {
	input := map[string]string{
		"name":  "Test",
		"phone": "+1234567890",
	}
	expected := "**New test**\n\n* name: Test\n* phone: +1234567890\n"

	actual := NewRoot().ExecuteMarkdown(name, input)

	suite.Equal(expected, actual)
}

func (suite *RootExtensionSuite) TestExecuteHTML() {
	input := map[string]string{
		"email": "test@test.com",
		"name":  "Test",
		"phone": "+1234567890",
	}
	expected := "<b>New test</b> by test@test.com<br>email: test@test.com<br>name: Test<br>phone: +1234567890<br>"

	actual := NewRoot().ExecuteHTML(name, input)

	suite.Equal(expected, actual)
}

func (suite *RootExtensionSuite) TestExecuteHTML_WithoutEmail() {
	input := map[string]string{
		"name":  "Test",
		"phone": "+1234567890",
	}
	expected := "<b>New test</b><br>name: Test<br>phone: +1234567890<br>"

	actual := NewRoot().ExecuteHTML(name, input)

	suite.Equal(expected, actual)
}

func TestRootExtensionSuite(t *testing.T) {
	suite.Run(t, new(RootExtensionSuite))
}
