package formext

import (
	"testing"

	"github.com/stretchr/testify/suite"

	"gitlab.com/etke.cc/miounne/v2/mocks"
)

const name = "test"

type FormExtensionSuite struct {
	suite.Suite
}

func (suite *FormExtensionSuite) SetupTest() {
	suite.T().Helper()
}

func (suite *FormExtensionSuite) TearDownTest() {
	suite.T().Helper()
}

func (suite *FormExtensionSuite) TestNewExtensions() {
	exts := NewExtensions(new(mocks.NetworkValidator))

	suite.IsType(&root{}, exts["root"])
}

func (suite *FormExtensionSuite) TestNewRoot() {
	actual := NewRoot()

	suite.IsType(&root{}, actual)
}

func (suite *FormExtensionSuite) TestSortKeys() {
	input := map[string]string{"c": "", "d": "", "a": ""}
	expected := []string{"a", "c", "d"}

	actual := sortKeys(input)

	suite.Equal(expected, actual)
}

func TestFormExtensionSuite(t *testing.T) {
	suite.Run(t, new(FormExtensionSuite))
}
