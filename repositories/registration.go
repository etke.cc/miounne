package repositories

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"maunium.net/go/mautrix/id"

	"gitlab.com/etke.cc/miounne/v2/models"
)

// Registration matrix-registration integration
type Registration struct {
	url       string
	secret    string
	roomID    id.RoomID
	notifyNew bool
	notifyUse bool
	client    *http.Client
}

type registrationErrorResponse struct {
	Code  string `json:"errcode"`
	Error string `json:"error"`
}

type registrationStatusResponse struct {
	models.RegistrationToken
}

type registrationListResponse []models.RegistrationToken

type registrationCreateTokenRequest struct {
	MaxUsage int    `json:"max_usage,omitempty"`
	Once     bool   `json:"one_time,omitempty"` // for older versions
	Expires  string `json:"expiration_date,omitempty"`
	ExDate   string `json:"ex_date,omitempty"` // for older versions
	Empty    bool   `json:"empty"`             // for older versions, otherwise 500 error
}

type registrationDisableTokenRequest struct {
	Active  bool `json:"active"`
	Disable bool `json:"disable"` // for older versions
}

// NewRegistration object
func NewRegistration(url string, secret string, roomID id.RoomID, notifyNew bool, notifyUse bool) *Registration {
	return &Registration{url: url, secret: secret, roomID: roomID, notifyNew: notifyNew, notifyUse: notifyUse, client: http.DefaultClient}
}

// GetRoomID of control room
func (c *Registration) GetRoomID() id.RoomID {
	return c.roomID
}

// GetNotifyNew - notify about new tokens created
func (c *Registration) GetNotifyNew() bool {
	return c.notifyNew
}

// GetNotifyUse - notify about used tokens
func (c *Registration) GetNotifyUse() bool {
	return c.notifyUse
}

func (c *Registration) sendRequest(method, url string, body io.Reader) (*http.Response, error) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Authorization", "SharedSecret "+c.secret)
	req.Header.Add("Accept", "application/json")
	if method != "GET" {
		req.Header.Add("Content-Type", "application/json")
	}

	resp, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}

	return resp, err
}

// readError from matrix-registration response
func (c *Registration) readError(status int, body []byte) error {
	errResponse := registrationErrorResponse{}
	if err := json.Unmarshal(body, &errResponse); err == nil && errResponse.Code != "" && errResponse.Error != "" {
		return fmt.Errorf("%s: %s", errResponse.Code, errResponse.Error)
	}

	if status == http.StatusNotFound {
		return fmt.Errorf("matrix-registration not found, check the API url")
	}

	return nil
}

// GetStatus of matrix-registration token
func (c *Registration) GetStatus(token string) (*models.RegistrationToken, error) {
	resp, err := c.sendRequest("GET", c.url+"/token/"+token, nil)
	if err != nil {
		return nil, err
	}

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	err = c.readError(resp.StatusCode, bodyBytes)
	if err != nil {
		return nil, err
	}

	statusResponse := registrationStatusResponse{}
	err = json.Unmarshal(bodyBytes, &statusResponse)
	if err != nil {
		return nil, err
	}

	return &statusResponse.RegistrationToken, nil
}

// GetList of invite tokens
func (c *Registration) GetList() ([]models.RegistrationToken, error) {
	resp, err := c.sendRequest("GET", c.url+"/token", nil)
	if err != nil {
		return nil, err
	}
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	err = c.readError(resp.StatusCode, bodyBytes)
	if err != nil {
		return nil, err
	}

	listResponse := registrationListResponse{}
	err = json.Unmarshal(bodyBytes, &listResponse)
	if err != nil {
		return nil, err
	}

	return listResponse, nil
}

// DisableToken ...
func (c *Registration) DisableToken(token string) (*models.RegistrationToken, error) {
	request := &registrationDisableTokenRequest{Active: false, Disable: true}
	requestBytes, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	resp, err := c.sendRequest("PATCH", c.url+"/token/"+token, bytes.NewBuffer(requestBytes))
	// yes, older version. Yes, different http verb and body. Yes, older MINOR version. Don't ask me anything...
	if resp.StatusCode == http.StatusMethodNotAllowed {
		resp, err = c.sendRequest("PUT", c.url+"/token/"+token, bytes.NewBuffer(requestBytes))
	}
	if err != nil {
		return nil, err
	}
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	err = c.readError(resp.StatusCode, bodyBytes)
	if err != nil {
		return nil, err
	}

	statusResponse := registrationStatusResponse{}
	err = json.Unmarshal(bodyBytes, &statusResponse)
	if err != nil {
		return nil, err
	}

	return &statusResponse.RegistrationToken, nil
}

func (c *Registration) createTokenRequest(maxUsage, expirationDate string) (*registrationCreateTokenRequest, error) {
	request := &registrationCreateTokenRequest{}

	if maxUsage != "" {
		maxUsageInt, err := strconv.Atoi(maxUsage)
		if err != nil {
			return nil, err
		}
		request.MaxUsage = maxUsageInt
		if maxUsageInt == 1 {
			request.Once = true
		}
	}

	if expirationDate != "" {
		_, err := time.Parse("2006-01-02", expirationDate)
		if err != nil {
			return nil, err
		}
		request.Expires = expirationDate
		request.ExDate = expirationDate
	}

	return request, nil
}

// CreateToken ...
func (c *Registration) CreateToken(maxUsage, expirationDate string) (*models.RegistrationToken, error) {
	request, err := c.createTokenRequest(maxUsage, expirationDate)
	if err != nil {
		return nil, err
	}

	requestBytes, err := json.Marshal(request)
	if err != nil {
		return nil, err
	}

	resp, err := c.sendRequest("POST", c.url+"/token", bytes.NewBuffer(requestBytes))
	if err != nil {
		return nil, err
	}
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	err = c.readError(resp.StatusCode, bodyBytes)
	if err != nil {
		return nil, err
	}

	statusResponse := registrationStatusResponse{}
	err = json.Unmarshal(bodyBytes, &statusResponse)
	if err != nil {
		return nil, err
	}

	return &statusResponse.RegistrationToken, nil
}
