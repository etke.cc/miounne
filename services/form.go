package services

import (
	"bytes"
	"fmt"
	"html/template"
	"net/http"
	"regexp"
	"strings"

	"maunium.net/go/mautrix/id"

	"gitlab.com/etke.cc/miounne/v2/configuration"
	"gitlab.com/etke.cc/miounne/v2/repositories/formext"
	"gitlab.com/etke.cc/miounne/v2/utils"
)

type formHandler struct {
	forms       []*configuration.Form
	policy      Policy
	spam        configuration.Spam
	sender      FormSender
	log         Logger
	exts        FormExtensions
	nv          utils.NetworkValidator
	redirectTpl *template.Template
}

// FormExtensions map
type FormExtensions map[string]formext.Extension

// FormHandler ...
type FormHandler interface {
	HandleGET(string, *http.Request) (string, error)
	HandlePOST(string, *http.Request) (string, error)
}

// FormSender is object that sends forms
type FormSender interface {
	SendWebhook(string, string, string, string, string) error
	SendMessage(id.RoomID, string) error
}

// based on W3C email regex, ref: https://www.w3.org/TR/2016/REC-html51-20161101/sec-forms.html#email-state-typeemail
var (
	emailRegex  = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	domainRegex = regexp.MustCompile(`^(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z0-9][a-zA-Z0-9-]{0,61}[a-zA-Z0-9]$`)
)

// NewFormHandler creates FormHandler object
func NewFormHandler(forms []*configuration.Form, exts FormExtensions, spam configuration.Spam, netvalid utils.NetworkValidator, policy Policy, sender FormSender, log Logger) FormHandler {
	// a bit hacky way to set redirect template
	redirectTpl := template.Must(template.New("redirect").Parse("<html><head><title>Redirecting...</title><meta http-equiv=\"Refresh\" content=\"0; url='{{ .URL }}'\" /></head><body>Redirecting to <a href='{{ .URL }}'>{{ .URL }}</a>..."))

	return &formHandler{forms: forms, exts: exts, spam: spam, nv: netvalid, policy: policy, sender: sender, log: log, redirectTpl: redirectTpl}
}

// redirect returns HTML redirect template
func (handler *formHandler) redirect(target string) (string, error) {
	var html bytes.Buffer
	data := struct {
		URL string
	}{
		URL: target,
	}
	err := handler.redirectTpl.Execute(&html, data)

	return html.String(), err
}

// HandleGET request, because some browsers trying to GET form before POST it in rare cases
func (handler *formHandler) HandleGET(name string, r *http.Request) (string, error) {
	form, notFoundErr := handler.findForm(name)
	if notFoundErr != nil {
		return "", notFoundErr
	}

	return handler.redirect(form.Redirect)
}

// HandlePOST request
func (handler *formHandler) HandlePOST(name string, r *http.Request) (string, error) {
	form, notFoundErr := handler.findForm(name)
	if notFoundErr != nil {
		return "", notFoundErr
	}

	if handler.isSpam(form.Name, r.PostFormValue("domain"), r.PostFormValue("email"), r.PostFormValue(form.Honeypot)) {
		return handler.redirect(form.Redirect)
	}

	data := make(map[string]string, len(form.Fields))
	for _, field := range form.Fields {
		data[field] = strings.TrimSpace(handler.policy.Sanitize(r.PostFormValue(field)))
	}

	markdown, html := handler.generate(form, data)

	if form.RoomID != "" {
		err := handler.sender.SendMessage(form.RoomID, markdown)
		if err != nil {
			handler.log.Error("cannot send matrix message: %v", err)
			return "", err
		}
	}
	if form.URL != "" {

		err := handler.sender.SendWebhook(form.Img, "Mother Miounne", "html", html, form.URL)
		if err != nil {
			handler.log.Error("cannot send webhook: %v", err)
			return "", err
		}
	}

	return handler.redirect(form.Redirect)
}

// generate markdown and html otput
func (handler *formHandler) generate(form *configuration.Form, data map[string]string) (string, string) {
	markdown := handler.exts["root"].ExecuteMarkdown(form.Name, data)
	for _, ext := range form.Extensions {
		if ext == "" {
			continue
		}
		markdown += "\n"
		markdown += handler.exts[ext].ExecuteMarkdown(form.Name, data)
	}

	html := handler.exts["root"].ExecuteHTML(form.Name, data)
	for _, ext := range form.Extensions {
		if ext == "" {
			continue
		}
		html += "<br>"
		html += handler.exts[ext].ExecuteHTML(form.Name, data)
	}

	return markdown, html
}

func (handler *formHandler) findForm(name string) (*configuration.Form, error) {
	for _, item := range handler.forms {
		if item.Name == name {
			return item, nil
		}
	}

	return nil, fmt.Errorf("form not found")
}

// isSpam - check if submission is a spam
func (handler *formHandler) isSpam(name, domain, email, honeypot string) bool {
	// If honeypot enabled and POSTed value is set, consider that submission as spam
	if honeypot != "" {
		handler.log.Warn("spam submission to %s from %s reason: honeypot", name, email)
		return true
	}

	if domain != "" && handler.isDomainInvalid(domain) {
		handler.log.Warn("spam submission to %s from %s reason: domain", name, email)
		return true
	}

	// If email (or part of it) exists in spam list, consider that submission as spam
	if email != "" && handler.isEmailInvalid(email) {
		handler.log.Warn("spam submission to %s from %s reason: spam config", name, email)
		return true
	}

	return false
}

func (handler *formHandler) isDomainInvalid(domain string) bool {
	if len(domain) < 4 || len(domain) > 77 {
		return true
	}

	return !domainRegex.MatchString(domain)
}

// checks if email address, email host or part of email host is spammer
func (handler *formHandler) isEmailInvalid(email string) bool {
	// email cannot too short and too big
	if len(email) < 3 || len(email) > 254 {
		return true
	}

	// check format
	if !emailRegex.MatchString(email) {
		return true
	}

	at := strings.LastIndex(email, "@")
	host := email[at+1:]
	domain := utils.GetBaseDomain(host)

	// If full email address listed in spam list, consider submission as spam
	for _, address := range handler.spam.Emails {
		if address == email {
			return true
		}
	}

	return handler.isProviderInvalid(host, domain)
}

func (handler *formHandler) isProviderInvalid(host, domain string) bool {
	// If hostname or base domain listed in spam list, consider submission as spam
	for _, hostname := range handler.spam.Hosts {
		if hostname == host || hostname == domain {
			return true
		}
	}

	// if neither provided host (maybe with subdomains) nor base domain has MX entries - mark it as invalid
	return !handler.nv.HasMX(host) && !handler.nv.HasMX(domain)
}
