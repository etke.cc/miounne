package services

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"maunium.net/go/mautrix/event"
	"maunium.net/go/mautrix/format"
	"maunium.net/go/mautrix/id"
)

type webhookRequest struct {
	Text        string `json:"text"`
	Format      string `json:"format"`
	DisplayName string `json:"displayName"`
	AvatarURL   string `json:"avatarUrl"`
}

type webhookResponse struct {
	Success bool `json:"success"`
}

type bot struct {
	frequency    time.Duration
	bmc          BMC
	client       MatrixClient
	logger       Logger
	registration MatrixRegistrationClient
	syncer       MatrixSyncer
	ticker       *time.Ticker
	startedAt    int64
	userID       id.UserID
	deviceID     id.DeviceID
}

// MatrixBot ...
type MatrixBot interface {
	SendMessage(roomID id.RoomID, message string) error
	SendWebhook(avatar, name, format, text, url string) error
	Start(displayName string)
	Stop()
}

var allowedCommands = []string{"ping", "!ping", "help", "registration", "bmc"}

// NewMatrixBot ...
func NewMatrixBot(client MatrixClient, syncer MatrixSyncer, logger Logger, frequency int, registration MatrixRegistrationClient, bmc BMC) MatrixBot {
	return &bot{client: client, syncer: syncer, logger: logger, frequency: time.Duration(frequency) * time.Minute, registration: registration, bmc: bmc}
}

// Start matrix bot
func (b *bot) Start(displayName string) {
	b.ticker = time.NewTicker(b.frequency)
	b.startedAt = time.Now().UnixMilli()
	if err := b.setProfile(displayName); err != nil {
		b.logger.Error("cannot set profile: %v", err)
	}
	go b.sync()
	go b.tick()
}

// Stop matrix bot
func (b *bot) Stop() {
	err := b.client.SetPresence(event.PresenceOffline)
	if err != nil {
		b.logger.Error("cannot set presence to offline: %v", err)
	}

	_, err = b.client.Logout()
	if err != nil {
		b.logger.Error("cannot logout: %v", err)
	}

	b.ticker.Stop()
}

// SendMessage in markdown format
func (b *bot) SendMessage(roomID id.RoomID, message string) error {
	content := format.RenderMarkdown(message, true, false)
	size := len(content.Body)
	overflow := (size + MatrixInfrastructurePayloadSize) - MatrixMaxPayloadSize
	if overflow > 0 {
		note := "\n\n> **NOTE**: that message was shrunk, because original size is more than allowed"
		shrinkpoint := size - overflow - len(note)
		content.Body = content.Body[shrinkpoint:] + note
	}
	_, err := b.client.SendMessageEvent(roomID, event.EventMessage, &content)
	return err
}

// SendWebhook to matrix room
func (b *bot) SendWebhook(avatar, name, format, text, url string) error {
	request := &webhookRequest{
		AvatarURL:   avatar,
		DisplayName: name,
		Format:      format,
		Text:        text,
	}

	requestBytes, err := json.Marshal(request)
	if err != nil {
		return fmt.Errorf("json.Marshal of request body failed: %v", err)
	}

	resp, err := http.Post(url, "application/json", bytes.NewBuffer(requestBytes))
	if err != nil {
		return fmt.Errorf("http.Post to matrix webhooks failed: %v", err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("ioutil.ReadAll of response body failed: %v", err)
	}

	response := &webhookResponse{}
	err = json.Unmarshal(body, response)
	if err != nil {
		return fmt.Errorf("json.Unmarshal of matrix webhooks response failed: %v; body: %s", err, string(body))
	}

	if !response.Success {
		return fmt.Errorf("matrix webhooks response has success = false, body: %s", string(body))
	}

	return nil
}

func (b *bot) setProfile(displayName string) error {
	whoami, err := b.client.Whoami()
	if err != nil {
		return fmt.Errorf("cannot get whoami: %v", err)
	}
	b.userID = whoami.UserID
	b.deviceID = whoami.DeviceID

	ownDisplayName, err := b.client.GetOwnDisplayName()
	if err != nil {
		return fmt.Errorf("cannot get display name: %v", err)
	}
	if ownDisplayName.DisplayName != displayName {
		err := b.client.SetDisplayName(displayName)
		if err != nil {
			return fmt.Errorf("cannot set display name: %v", err)
		}
	}

	return nil
}

func (b *bot) tick() {
	for range b.ticker.C {
		if b.bmc != nil {
			b.notifyBMCpurchases()
			b.notifyBMCsupporters()
			b.notifyBMCmembersActive()
			b.notifyBMCmembersInactive()
		}
		if b.registration != nil {
			b.notifyRegistrationTokens()
		}
	}
}

func (b *bot) handleCommand(room id.RoomID, evt *event.Event) string {
	slice := strings.Split(strings.TrimSpace(evt.Content.AsMessage().Body), " ")
	if !b.shouldHandleCommand(slice[0]) {
		return ""
	}
	_, err := b.client.UserTyping(room, true, typingTimeout)
	if err != nil {
		b.logger.Error("could not send typing notification to the room %s: %v", room, err)
	}
	// nolint // we don't care for error here
	defer b.client.UserTyping(room, false, typingTimeout)

	message := strings.Join(slice[1:], " ")
	// all command prefixes must be added in allowedCommands var
	switch slice[0] {
	case "ping", "!ping":
		return b.ping(evt.Timestamp, evt.Sender, message)
	case "help":
		return b.help()
	case "registration":
		return b.handleCommandRegistration(room, slice)
	case "bmc":
		return b.handleCommandBMC(room, slice)
	default:
		// as we have allowedCommands var, that 'default' case is just-in-case.
		return ""
	}
}

func (b *bot) shouldHandleCommand(command string) bool {
	for _, allowed := range allowedCommands {
		if command == allowed {
			return true
		}
	}

	return false
}

// handleCommandRegistration is the matrix-registration integration
func (b *bot) handleCommandRegistration(room id.RoomID, args []string) string {
	if b.registration == nil || len(args) == 1 {
		return b.help()
	}
	if b.registration.GetRoomID() != room {
		return b.wrongRoom()
	}

	switch args[1] {
	case "list":
		return b.registrationList()
	case "create":
		return b.registrationCreate(args)
	case "disable":
		return b.registrationDisable(args)
	case "status":
		return b.registrationStatus(args)
	default:
		return b.help()
	}
}

func (b *bot) handleCommandBMC(room id.RoomID, args []string) string {
	if b.bmc == nil || len(args) == 1 {
		return b.help()
	}
	if b.bmc.GetRoomID() != room {
		return b.wrongRoom()
	}

	switch args[1] {
	case "members":
		return b.bmcMembers(args)
	case "supporters":
		return b.bmcSupporters()
	case "extras":
		return b.bmcExtras()
	default:
		return b.help()
	}
}
