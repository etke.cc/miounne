package services

import (
	"context"
	"fmt"
	"time"

	"github.com/sethvargo/go-retry"
	"maunium.net/go/mautrix"
	"maunium.net/go/mautrix/event"
	"maunium.net/go/mautrix/id"

	"gitlab.com/etke.cc/miounne/v2/models"
	"gitlab.com/etke.cc/miounne/v2/repositories"
)

const (
	// MatrixMaxPayloadSize of any matrix event, in bytes
	MatrixMaxPayloadSize = 65536
	// MatrixInfrastructurePayloadSize is an approximate size in bytes of json payload template, may differ a bit by metadata size
	MatrixInfrastructurePayloadSize = 1000

	accountDataPrefix = "cc.etke.miounne."
	typingTimeout     = int64(10_000_000) // ms
)

// MatrixClient - matrix client-server API client interface
type MatrixClient interface {
	GetAccountData(string, interface{}) error
	SetAccountData(string, interface{}) error
	UserTyping(id.RoomID, bool, int64) (*mautrix.RespTyping, error)
	SendMessageEvent(id.RoomID, event.Type, interface{}, ...mautrix.ReqSendEvent) (*mautrix.RespSendEvent, error)
	GetOwnDisplayName() (*mautrix.RespUserDisplayName, error)
	SetDisplayName(string) error
	SetPresence(event.Presence) error
	JoinRoomByID(id.RoomID) (*mautrix.RespJoinRoom, error)
	Whoami() (*mautrix.RespWhoami, error)
	Sync() error
	Logout() (*mautrix.RespLogout, error)
}

// MatrixSyncer is a wrapper around mautrix default syncer
type MatrixSyncer interface {
	mautrix.ExtensibleSyncer
}

// MatrixRegistrationClient - matrix-registration API client
type MatrixRegistrationClient interface {
	CreateToken(string, string) (*models.RegistrationToken, error)
	DisableToken(string) (*models.RegistrationToken, error)
	GetList() ([]models.RegistrationToken, error)
	GetRoomID() id.RoomID
	GetNotifyNew() bool
	GetNotifyUse() bool
	GetStatus(string) (*models.RegistrationToken, error)
}

// NewMatrixClient - creates matrix client-server API client
func NewMatrixClient(homeserver, username, password string) (MatrixClient, MatrixSyncer, error) {
	ctx := context.Background()
	client, newErr := mautrix.NewClient(homeserver, "", "")
	if newErr != nil {
		return nil, nil, newErr
	}

	retryErr := retry.Fibonacci(ctx, 1*time.Second, func(_ context.Context) error {
		_, err := client.Login(&mautrix.ReqLogin{
			Type: "m.login.password",
			Identifier: mautrix.UserIdentifier{
				Type: mautrix.IdentifierTypeUser,
				User: username,
			},
			Password:         password,
			StoreCredentials: true,
		})
		if err != nil {
			return retry.RetryableError(err)
		}
		return nil
	})

	if retryErr != nil {
		return nil, nil, fmt.Errorf("cannot login, even after retries: %v", retryErr)
	}

	accountDataKey := accountDataPrefix + "batch_token"
	client.Store = mautrix.NewAccountDataStore(accountDataKey, client)
	filter := client.Syncer.GetFilterJSON(client.UserID)
	filter.AccountData = mautrix.FilterPart{
		Limit: 50,
		NotTypes: []event.Type{
			event.NewEventType(accountDataKey),
		},
	}

	filterResp, err := client.CreateFilter(filter)
	if err != nil {
		return nil, nil, fmt.Errorf("cannot create the account data filter (%s): %v", accountDataKey, err)
	}
	client.Store.SaveFilterID(client.UserID, filterResp.FilterID)

	return client, client.Syncer.(*mautrix.DefaultSyncer), nil
}

// NewMatrixRegistrationClient returns matrix-registration API client
func NewMatrixRegistrationClient(url string, secret string, roomID id.RoomID, notifyNew bool, notifyUse bool) MatrixRegistrationClient {
	return repositories.NewRegistration(url, secret, roomID, notifyNew, notifyUse)
}
