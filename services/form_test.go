package services

import (
	"errors"
	"net/http"
	"net/url"
	"strings"
	"testing"

	"github.com/microcosm-cc/bluemonday"
	"github.com/stretchr/testify/suite"
	"maunium.net/go/mautrix/id"

	"gitlab.com/etke.cc/miounne/v2/configuration"
	"gitlab.com/etke.cc/miounne/v2/mocks"
	"gitlab.com/etke.cc/miounne/v2/repositories/formext"
	"gitlab.com/etke.cc/miounne/v2/utils"
)

type FormSuite struct {
	suite.Suite
	name                string
	defaultTarget       string
	defaultRedirectHTML string
	handler             *formHandler
	sender              *mocks.FormSender
	nv                  *mocks.NetworkValidator
}

func (suite *FormSuite) SetupTest() {
	suite.T().Helper()

	suite.name = "test"
	suite.defaultTarget = "https://example.org"
	spam := configuration.Spam{
		Hosts:  []string{"spam.com"},
		Emails: []string{"spammer@example.com"},
	}
	config := []*configuration.Form{
		{
			Name:       suite.name,
			Redirect:   suite.defaultTarget,
			Fields:     []string{"email", "name"},
			Honeypot:   "honeypot",
			URL:        "https://example.com/webhook",
			RoomID:     "!test@example.com",
			Extensions: []string{"example"},
		},
	}
	policy := bluemonday.StrictPolicy()
	suite.sender = new(mocks.FormSender)
	suite.nv = new(mocks.NetworkValidator)
	log := utils.NewLogger("forms.", "INFO")
	exts := formext.NewExtensions(suite.nv)

	suite.handler = NewFormHandler(config, exts, spam, suite.nv, policy, suite.sender, log).(*formHandler)
	suite.defaultRedirectHTML = "<html><head><title>Redirecting...</title><meta http-equiv=\"Refresh\" content=\"0; url='https://example.org'\" /></head><body>Redirecting to <a href='https://example.org'>https://example.org</a>..."
}

func (suite *FormSuite) TestRedirect() {
	target := suite.defaultTarget
	expected := suite.defaultRedirectHTML

	html, err := suite.handler.redirect(target)

	suite.NoError(err)
	suite.Equal(expected, html)
}

func (suite *FormSuite) TestHandleGET() {
	request, requestErr := http.NewRequest("GET", "https://example.com", nil)
	expected := suite.defaultRedirectHTML

	html, err := suite.handler.HandleGET(suite.name, request)

	suite.NoError(err)
	suite.NoError(requestErr)
	suite.Equal(expected, html)
}

func (suite *FormSuite) TestHandleGET_NotFound() {
	name := "doesnt exist"
	request, requestErr := http.NewRequest("GET", "https://example.com", nil)

	html, err := suite.handler.HandleGET(name, request)

	suite.Error(err)
	suite.Empty(html)
	suite.Equal(errors.New("form not found"), err)
	suite.NoError(requestErr)
}

func (suite *FormSuite) TestHandlePOST() {
	expected := suite.defaultRedirectHTML
	suite.nv.On("HasMX", "test.example.com").Return(true)
	suite.nv.On("HasMX", "example.com").Return(true)
	form := url.Values{}
	form.Add("email", "test@test.example.com")
	form.Add("domain", "test.example.com")
	form.Add("name", "")
	form.Add("honeypot", "")
	request, requestErr := http.NewRequest("POST", "https://example.com", strings.NewReader(form.Encode()))
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	suite.sender.On("SendWebhook", "", "Mother Miounne", "html", "<b>New test</b> by test@test.example.com<br>email: test@test.example.com<br><br><b>Example</b>", "https://example.com/webhook").Return(nil)
	suite.sender.On("SendMessage", id.RoomID("!test@example.com"), "**New test** by test@test.example.com\n\n* email: test@test.example.com\n\n**Example**").Return(nil)

	html, err := suite.handler.HandlePOST(suite.name, request)

	suite.NoError(err)
	suite.NoError(requestErr)
	suite.Equal(expected, html)
}

func (suite *FormSuite) TestHandlePOST_WithoutEmail() {
	expected := suite.defaultRedirectHTML
	form := url.Values{}
	form.Add("name", "Test")
	form.Add("domain", "example.com")
	form.Add("honeypot", "")
	request, requestErr := http.NewRequest("POST", "https://example.com", strings.NewReader(form.Encode()))
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	suite.sender.On("SendWebhook", "", "Mother Miounne", "html", "<b>New test</b><br>name: Test<br><br><b>Example</b>", "https://example.com/webhook").Return(nil)
	suite.sender.On("SendMessage", id.RoomID("!test@example.com"), "**New test**\n\n* name: Test\n\n**Example**").Return(nil)

	html, err := suite.handler.HandlePOST(suite.name, request)

	suite.NoError(err)
	suite.NoError(requestErr)
	suite.Equal(expected, html)
}

func (suite *FormSuite) TestHandlePOST_NotFound() {
	name := "doesnt exist"
	request, requestErr := http.NewRequest("POST", "https://example.com", nil)

	html, err := suite.handler.HandlePOST(name, request)

	suite.Error(err)
	suite.Empty(html)
	suite.Equal(errors.New("form not found"), err)
	suite.NoError(requestErr)
}

func (suite *FormSuite) TestHandlePOST_SpamHoneypot() {
	expected := suite.defaultRedirectHTML
	form := url.Values{}
	form.Add("email", "test@test.example.com")
	form.Add("domain", "example.com")
	form.Add("name", "Test")
	form.Add("honeypot", "im spammer!")
	request, requestErr := http.NewRequest("POST", "https://example.com", strings.NewReader(form.Encode()))
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	html, err := suite.handler.HandlePOST(suite.name, request)

	suite.NoError(err)
	suite.NoError(requestErr)
	suite.Equal(expected, html)
}

func (suite *FormSuite) TestHandlePOST_SpamHost() {
	expected := suite.defaultRedirectHTML
	form := url.Values{}
	form.Add("email", "test@spam.com")
	form.Add("domain", "example.com")
	form.Add("name", "Test")
	form.Add("honeypot", "")
	request, requestErr := http.NewRequest("POST", "https://example.com", strings.NewReader(form.Encode()))
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	html, err := suite.handler.HandlePOST(suite.name, request)

	suite.NoError(err)
	suite.NoError(requestErr)
	suite.Equal(expected, html)
}

func (suite *FormSuite) TestHandlePOST_SpamEmail() {
	expected := suite.defaultRedirectHTML
	form := url.Values{}
	form.Add("email", "spammer@example.com")
	form.Add("name", "Test")
	form.Add("honeypot", "")
	request, requestErr := http.NewRequest("POST", "https://example.com", strings.NewReader(form.Encode()))
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	html, err := suite.handler.HandlePOST(suite.name, request)

	suite.NoError(err)
	suite.NoError(requestErr)
	suite.Equal(expected, html)
}

func (suite *FormSuite) TestHandlePOST_WrongDomain() {
	expected := suite.defaultRedirectHTML
	form := url.Values{}
	form.Add("email", "legit@email.com")
	form.Add("domain", "invalid")
	form.Add("name", "Test")
	form.Add("honeypot", "")
	request, requestErr := http.NewRequest("POST", "https://example.com", strings.NewReader(form.Encode()))
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	html, err := suite.handler.HandlePOST(suite.name, request)

	suite.NoError(err)
	suite.NoError(requestErr)
	suite.Equal(expected, html)
}

func (suite *FormSuite) TestHandlePOST_WrongEmail() {
	expected := suite.defaultRedirectHTML
	form := url.Values{}
	form.Add("email", "spammer")
	form.Add("name", "Test")
	form.Add("honeypot", "")
	request, requestErr := http.NewRequest("POST", "https://example.com", strings.NewReader(form.Encode()))
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	html, err := suite.handler.HandlePOST(suite.name, request)

	suite.NoError(err)
	suite.NoError(requestErr)
	suite.Equal(expected, html)
}

func (suite *FormSuite) TestHandlePOST_WrongEmailShort() {
	expected := suite.defaultRedirectHTML
	form := url.Values{}
	form.Add("email", "..")
	form.Add("name", "Test")
	form.Add("honeypot", "")
	request, requestErr := http.NewRequest("POST", "https://example.com", strings.NewReader(form.Encode()))
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	html, err := suite.handler.HandlePOST(suite.name, request)

	suite.NoError(err)
	suite.NoError(requestErr)
	suite.Equal(expected, html)
}

func (suite *FormSuite) TestHandlePOST_WrongEmailHost() {
	suite.nv.On("HasMX", "test.example.com").Return(false)
	suite.nv.On("HasMX", "example.com").Return(false)
	expected := suite.defaultRedirectHTML
	form := url.Values{}
	form.Add("email", "spammer@test.example.com")
	form.Add("name", "Test")
	form.Add("honeypot", "")
	request, requestErr := http.NewRequest("POST", "https://example.com", strings.NewReader(form.Encode()))
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	html, err := suite.handler.HandlePOST(suite.name, request)

	suite.NoError(err)
	suite.NoError(requestErr)
	suite.Equal(expected, html)
}

func (suite *FormSuite) TestHandlePOST_WebhookError() {
	suite.nv.On("HasMX", "test.example.com").Return(true)
	suite.nv.On("HasMX", "example.com").Return(true)
	form := url.Values{}
	form.Add("email", "test@test.example.com")
	form.Add("name", "")
	form.Add("honeypot", "")
	suite.handler.forms[0].RoomID = ""
	request, requestErr := http.NewRequest("POST", "https://example.com", strings.NewReader(form.Encode()))
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	suite.sender.On("SendWebhook", "", "Mother Miounne", "html", "<b>New test</b> by test@test.example.com<br>email: test@test.example.com<br><br><b>Example</b>", "https://example.com/webhook").Return(errors.New("webhook error"))

	html, err := suite.handler.HandlePOST(suite.name, request)

	suite.Error(err)
	suite.Empty(html)
	suite.Equal(errors.New("webhook error"), err)
	suite.NoError(requestErr)
}

func (suite *FormSuite) TestHandlePOST_MatrixError() {
	suite.nv.On("HasMX", "test.example.com").Return(true)
	suite.nv.On("HasMX", "example.com").Return(true)
	form := url.Values{}
	form.Add("email", "test@test.example.com")
	form.Add("name", "")
	form.Add("honeypot", "")
	request, requestErr := http.NewRequest("POST", "https://example.com", strings.NewReader(form.Encode()))
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	suite.sender.On("SendWebhook", "", "Mother Miounne", "html", "<b>New test</b> by test@test.example.com<br>email: test@test.example.com<br><br><b>Example</b>", "https://example.com/webhook").Return(nil)
	suite.sender.On("SendMessage", id.RoomID("!test@example.com"), "**New test** by test@test.example.com\n\n* email: test@test.example.com\n\n**Example**").Return(errors.New("matrix error"))

	html, err := suite.handler.HandlePOST(suite.name, request)

	suite.Error(err)
	suite.Empty(html)
	suite.Equal(errors.New("matrix error"), err)
	suite.NoError(requestErr)
}

func TestFormHandlerSuite(t *testing.T) {
	suite.Run(t, new(FormSuite))
}
