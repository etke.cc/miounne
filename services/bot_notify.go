package services

import (
	"gitlab.com/etke.cc/miounne/v2/models"
)

const (
	eventBMCmembers        = accountDataPrefix + "bmc.members"
	eventBMCsupporters     = accountDataPrefix + "bmc.supporters"
	eventBMCpurchases      = accountDataPrefix + "bmc.purchases"
	eventRegisrationTokens = accountDataPrefix + "registration.tokens"
)

type accountDataIDs struct {
	IDs []int `json:"ids"`
}

type accountDataMSI map[string]int

func (b *bot) diffBMCmembers(status string) []*models.BMCMember {
	key := eventBMCmembers + "." + status
	var err error
	var diff []*models.BMCMember
	var old accountDataIDs
	err = b.client.GetAccountData(key, &old)
	if err != nil {
		b.logger.Warn("cannot get %s BMC members from account data: %v", status, err)
	}
	uniq := make(map[int]struct{}, len(old.IDs))
	for _, id := range old.IDs {
		uniq[id] = struct{}{}
	}

	current, err := b.bmc.GetMembers(status)
	if err != nil {
		b.logger.Error("cannot get %s BMC members from API: %v", status, err)
		return diff
	}
	currentIDs := accountDataIDs{IDs: make([]int, 0, len(current))}

	for _, member := range current {
		currentIDs.IDs = append(currentIDs.IDs, member.ID)
		if _, ok := uniq[member.ID]; !ok {
			diff = append(diff, member)
		}
	}

	err = b.client.SetAccountData(key, currentIDs)
	if err != nil {
		b.logger.Error("cannot set %s BMC members to account data: %v", status, err)
	}

	return diff
}

func (b *bot) notifyBMCmembersInactive() {
	b.logger.Debug("checking inactive bmc members to send notification")
	if !b.bmc.GetNotifyMembers() {
		return
	}

	diff := b.diffBMCmembers("inactive")
	if len(diff) == 0 {
		b.logger.Debug("no new inactive bmc members")
		return
	}

	message := "new **inactive** member\n\n" + b.bmcMembersView(diff)

	err := b.SendMessage(b.bmc.GetRoomID(), message)
	if err != nil {
		b.logger.Error("cannot send bmc inactive members notification: %v", err)
	}
}

func (b *bot) notifyBMCmembersActive() {
	b.logger.Debug("checking active bmc members to send notification")
	if !b.bmc.GetNotifyMembers() {
		return
	}

	diff := b.diffBMCmembers("active")
	if len(diff) == 0 {
		b.logger.Debug("no new active bmc members")
		return
	}

	message := "new **active** member\n\n" + b.bmcMembersView(diff)

	err := b.SendMessage(b.bmc.GetRoomID(), message)
	if err != nil {
		b.logger.Error("cannot send bmc active members notification: %v", err)
	}
}

func (b *bot) diffBMCsupporters() []*models.BMCSupporter {
	var err error
	var diff []*models.BMCSupporter
	var old accountDataIDs
	err = b.client.GetAccountData(eventBMCsupporters, &old)
	if err != nil {
		b.logger.Warn("cannot get BMC supporters from account data: %v", err)
	}
	uniq := make(map[int]struct{}, len(old.IDs))
	for _, id := range old.IDs {
		uniq[id] = struct{}{}
	}

	current, err := b.bmc.GetSupporters()
	if err != nil {
		b.logger.Error("cannot get BMC supporters from API: %v", err)
		return diff
	}
	currentIDs := accountDataIDs{IDs: make([]int, 0, len(current))}

	for _, supporter := range current {
		currentIDs.IDs = append(currentIDs.IDs, supporter.ID)
		if _, ok := uniq[supporter.ID]; !ok {
			diff = append(diff, supporter)
		}
	}

	err = b.client.SetAccountData(eventBMCsupporters, currentIDs)
	if err != nil {
		b.logger.Error("cannot set BMC supporters to account data: %v", err)
	}

	return diff
}

func (b *bot) notifyBMCsupporters() {
	b.logger.Debug("checking bmc supporters to send notification")
	if !b.bmc.GetNotifySupporters() {
		return
	}

	diff := b.diffBMCsupporters()
	if len(diff) == 0 {
		b.logger.Debug("no new bmc supporters")
		return
	}

	message := "new **supporter**\n\n" + b.bmcSupportersView(diff)

	err := b.SendMessage(b.bmc.GetRoomID(), message)
	if err != nil {
		b.logger.Error("cannot send bmc supporters notification: %v", err)
	}
}

func (b *bot) diffBMCpurchases() []*models.BMCPurchase {
	var err error
	var diff []*models.BMCPurchase
	var old accountDataIDs
	err = b.client.GetAccountData(eventBMCpurchases, &old)
	if err != nil {
		b.logger.Warn("cannot get BMC purchases from account data: %v", err)
	}
	uniq := make(map[int]struct{}, len(old.IDs))
	for _, id := range old.IDs {
		uniq[id] = struct{}{}
	}

	current, err := b.bmc.GetExtras()
	if err != nil {
		b.logger.Error("cannot get BMC purchases from API: %v", err)
		return diff
	}
	currentIDs := accountDataIDs{IDs: make([]int, 0, len(current))}

	for _, purchase := range current {
		currentIDs.IDs = append(currentIDs.IDs, purchase.ID)
		if _, ok := uniq[purchase.ID]; !ok {
			diff = append(diff, purchase)
		}
	}

	err = b.client.SetAccountData(eventBMCpurchases, currentIDs)
	if err != nil {
		b.logger.Error("cannot set BMC purchases to account data: %v", err)
	}

	return diff
}

func (b *bot) notifyBMCpurchases() {
	b.logger.Debug("checking bmc purchases to send notification")
	if !b.bmc.GetNotifyExtras() {
		return
	}

	diff := b.diffBMCpurchases()
	if len(diff) == 0 {
		b.logger.Debug("no new bmc purchases")
		return
	}

	message := "new **purchase**\n\n" + b.bmcExtrasView(diff)

	err := b.SendMessage(b.bmc.GetRoomID(), message)
	if err != nil {
		b.logger.Error("cannot send bmc purchases notification: %v", err)
	}
}

// diffRegistrationTokens returns 2 slices: first one contains new tokens, second one contains usage changes
func (b *bot) diffRegistrationTokens() ([]*models.RegistrationToken, []*models.RegistrationToken) {
	var err error
	var diffNew []*models.RegistrationToken
	var diffUse []*models.RegistrationToken
	var old accountDataMSI
	err = b.client.GetAccountData(eventRegisrationTokens, &old)
	if err != nil {
		b.logger.Warn("cannot get registration tokens from account data: %v", err)
	}

	current, err := b.registration.GetList()
	if err != nil {
		b.logger.Error("cannot get registration tokens from API: %v", err)
		return diffNew, diffUse
	}
	currentMSI := make(accountDataMSI, len(current))

	for _, token := range current {
		currentMSI[token.Name] = token.Used

		used, ok := old[token.Name]
		if !ok {
			diffNew = append(diffNew, &token)
			continue
		}
		if used > token.Used {
			diffUse = append(diffUse, &token)
		}
	}

	err = b.client.SetAccountData(eventRegisrationTokens, currentMSI)
	if err != nil {
		b.logger.Error("cannot set registration tokens to account data: %v", err)
	}

	return diffNew, diffUse
}

func (b *bot) sendRegistrationNotification(tokens []*models.RegistrationToken, message string) {
	if len(tokens) == 0 {
		return
	}
	for _, token := range tokens {
		message += b.registrationStatusView(token)
	}
	err := b.SendMessage(b.registration.GetRoomID(), message)
	if err != nil {
		b.logger.Error("cannot send registration tokens notification: %v", err)
	}
}

func (b *bot) notifyRegistrationTokens() {
	b.logger.Debug("checking registration tokens to send notification")
	if !b.registration.GetNotifyNew() && !b.registration.GetNotifyUse() {
		return
	}

	created, used := b.diffRegistrationTokens()

	if b.registration.GetNotifyNew() {
		b.sendRegistrationNotification(created, "token has been **created**\n\n")
	}

	if b.registration.GetNotifyUse() {
		b.sendRegistrationNotification(used, "token has been **used**\n\n")
	}
}
