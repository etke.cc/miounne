package services

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"maunium.net/go/mautrix/id"

	"gitlab.com/etke.cc/miounne/v2/models"
	"gitlab.com/etke.cc/miounne/v2/utils"
)

// Simple ping/pong
func (b *bot) ping(timestamp int64, sender id.UserID, message string) string {
	suffix := "ms"
	now := time.Now().UnixMilli()
	diff := float64(utils.Abs(now - timestamp))
	switch {
	case diff >= 1000 && diff < 60000:
		diff /= 1000
		suffix = "sec"
	case diff >= 60000:
		diff /= 60000
		suffix = "min"
	}
	duration := strings.ReplaceAll(strconv.FormatFloat(diff, 'f', 1, 64), ".0", "")
	measurement := duration + suffix
	response := sender.String() + ": Pong!"
	if message != "" {
		response += " (ping " + message + " took " + measurement + " to arrive)"
	} else {
		response += " (took " + measurement + " to arrive)"
	}
	return response
}

func (b *bot) help() string {
	return `
General _available by default_:

* **ping** - simple ping/pong, consider it as Mother Miounne healthcheck
* **help** - you're reading it

Registration _only if [matrix-registration](https://github.com/ZerataX/matrix-registration) integration enabled_:

* **registration list** - list all invite tokens
* **registration create** [MAX_USAGE] [EXPIRATION_DATE]- create new invite token (params are optional), eg: "registration create 1 2021-12-31" (can be used only once, before 31 Jan 2021) or "registration create" (no restrictions)
* **registration disable** TOKEN - disable the TOKEN
* **registration status** TOKEN - get info about the TOKEN

BMC _only if [buymeacoffee.com](https://buymeacoffee.com) integration enabled_:

* **bmc members** [STATUS] - list of bmc subscribers/members, STATUS is one of: active, inactive, all (default)
* **bmc supporters** - list of bmc one-time supporters
* **bmc extras** - list of extras purchases
`
}

func (b *bot) wrongRoom() string {
	return "That's not the admin room, I'm going to pretend that nothing was sent."
}

func (b *bot) registrationStatusView(status *models.RegistrationToken) string {
	response := ""
	status.Finalize()
	response += "Token **" + status.Name + "**\n\n"
	if status.Active {
		response += "* active: yes\n"
	} else {
		response += "* active: no\n"
	}

	if status.Expires != "" {
		response += "* expires: " + status.Expires + "\n"
	}
	if status.MaxUsage > 0 {
		response += "* max usage: " + strconv.Itoa(status.MaxUsage) + "\n"
	}
	if status.Active && status.Used > 0 {
		response += "* already used: " + strconv.Itoa(status.Used) + "\n"
	}
	response += "\n\n"

	return response
}

func (b *bot) registrationList() string {
	var response string
	statuses, err := b.registration.GetList()
	if err != nil {
		return fmt.Sprintf("**Error**: %v", err)
	}

	if len(statuses) == 0 {
		return "No tokens available, you should create one first"
	}

	for _, status := range statuses {
		response += b.registrationStatusView(&status)
	}

	return response
}

func (b *bot) registrationCreate(args []string) string {
	var maxUsage, expirationDate string

	if len(args) >= 3 {
		maxUsage = args[2]
	}

	if len(args) >= 4 {
		expirationDate = args[3]
	}

	status, err := b.registration.CreateToken(maxUsage, expirationDate)
	if err != nil {
		return fmt.Sprintf("**Error**: %v", err)
	}
	return "New token: **" + status.Name + "**"
}

func (b *bot) registrationDisable(args []string) string {
	if len(args) < 3 {
		return "**Error**: you should send the token, eg: registration disable DoubleWizardSki"
	}
	token := args[2]
	status, err := b.registration.DisableToken(token)
	if err != nil {
		return fmt.Sprintf("**Error**: %v", err)
	}
	return "Token **" + status.Name + "** has been disabled"
}

func (b *bot) registrationStatus(args []string) string {
	if len(args) < 3 {
		return "**Error**: you should send the token, eg: registration status DoubleWizardSki"
	}

	token := args[2]
	status, err := b.registration.GetStatus(token)
	if err != nil {
		return fmt.Sprintf("**Error**: %v", err)
	}
	return b.registrationStatusView(status)
}

func (b *bot) bmcMembers(args []string) string {
	status := "all"
	if len(args) >= 3 {
		status = args[2]
	}
	members, err := b.bmc.GetMembers(status)
	if err != nil {
		return "**Error**: " + err.Error()
	}

	return b.bmcMembersView(members)
}

func (b *bot) bmcMembersView(members []*models.BMCMember) string {
	var result strings.Builder
	for _, member := range members {
		mstatus := ""
		price := strconv.FormatFloat(member.Price*float64(member.Quantity), 'f', -1, 64)
		if member.IsCanceled {
			mstatus = "(inactive)"
		}

		result.WriteString("**" + member.Name + "** <" + member.Email + "> " + mstatus + "\n\n")
		result.WriteString("* joined: " + member.CreatedAt + "\n")
		if member.IsCanceled {
			result.WriteString("* canceled: " + member.CanceledAt + "\n")
		}
		result.WriteString("* price: " + price + "/" + member.Duration + " (" + member.Currency + ")\n")
		if member.Message != "" {
			result.WriteString("* message: " + member.Message + "\n")
		}
		result.WriteString("\n\n")
	}

	return result.String()
}

func (b *bot) bmcSupporters() string {
	supporters, err := b.bmc.GetSupporters()
	if err != nil {
		return "**Error**: " + err.Error()
	}

	return b.bmcSupportersView(supporters)
}

func (b *bot) bmcSupportersView(supporters []*models.BMCSupporter) string {
	var result strings.Builder
	for _, supporter := range supporters {
		mstatus := ""
		price := strconv.FormatFloat(supporter.Price*float64(supporter.Quantity), 'f', -1, 64)
		if supporter.IsRefunded {
			mstatus = "(inactive)"
		}

		result.WriteString("**" + supporter.Name + "** <" + supporter.Email + "> " + mstatus + "\n\n")
		result.WriteString("* supported: " + supporter.CreatedAt + "\n")
		result.WriteString("* price: " + price + " " + supporter.Currency + "\n")
		if supporter.Message != "" {
			result.WriteString("* message: " + supporter.Message + "\n")
		}
		result.WriteString("\n\n")
	}

	return result.String()
}

func (b *bot) bmcExtras() string {
	extras, err := b.bmc.GetExtras()
	if err != nil {
		return "**Error**: " + err.Error()
	}

	return b.bmcExtrasView(extras)
}

func (b *bot) bmcExtrasView(extras []*models.BMCPurchase) string {
	var result strings.Builder
	for _, extra := range extras {
		mstatus := ""
		price := strconv.FormatFloat(extra.Price, 'f', -1, 64)
		if extra.IsRevoked {
			mstatus = "(revoked)"
		}

		result.WriteString("**" + extra.Name + "** <" + extra.Email + "> " + mstatus + "\n\n")
		result.WriteString("* item: " + extra.Title + "\n")
		result.WriteString("* paid: " + extra.CreatedAt + "\n")
		result.WriteString("* price: " + price + " " + extra.Currency + "\n")
		if extra.Message != "" {
			result.WriteString("* message: " + extra.Message + "\n")
		}
		result.WriteString("\n\n")
	}

	return result.String()
}
