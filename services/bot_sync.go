package services

import (
	"maunium.net/go/mautrix"
	"maunium.net/go/mautrix/event"
)

func (b *bot) syncOnInviteEvent(_ mautrix.EventSource, evt *event.Event) {
	userID := b.userID.String()
	invite := evt.Content.AsMember().Membership == event.MembershipInvite
	b.logger.Debug("sync, invite event = %t", invite)
	if invite && evt.GetStateKey() == userID {
		_, err := b.client.JoinRoomByID(evt.RoomID)
		if err != nil {
			b.logger.Error("cannot join room: %v", err)
		}
	}
}

func (b *bot) syncOnMessage(_ mautrix.EventSource, evt *event.Event) {
	// ignore messages, received before bot start
	if evt.Timestamp < b.startedAt {
		return
	}

	// ignore own messages
	if evt.Sender == b.userID {
		return
	}

	content := evt.Content.AsMessage()
	response := b.handleCommand(evt.RoomID, evt)
	b.logger.Debug("bot: new message in %s from %s: %s", evt.RoomID, evt.Sender, content.Body)
	b.logger.Trace("bot: response: %s", response)

	if response != "" {
		if err := b.SendMessage(evt.RoomID, response); err != nil {
			b.logger.Error("cannot send message: %v", err)
		}
	}
}

func (b *bot) sync() {
	b.syncer.OnEventType(event.StateMember, b.syncOnInviteEvent)
	b.syncer.OnEventType(event.EventMessage, b.syncOnMessage)

	if err := b.client.Sync(); err != nil {
		b.logger.Error("cannot sync: %v", err)
	}
}
