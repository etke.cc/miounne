package services

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type ServicesSuite struct {
	suite.Suite
}

func (suite *ServicesSuite) SetupTest() {
	suite.T().Helper()
}

func TestServicesSuite(t *testing.T) {
	suite.Run(t, new(ServicesSuite))
}
