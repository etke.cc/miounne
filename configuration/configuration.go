// Package configuration represents config management
package configuration

import (
	"fmt"
	"os"
	"strconv"
	"strings"

	"maunium.net/go/mautrix/id"
)

const prefix = "miounne"

func readEnv(shortkey, defaultValue string) string {
	key := strings.ToUpper(prefix + "_" + strings.ReplaceAll(shortkey, ".", "_"))
	value := strings.TrimSpace(os.Getenv(key))
	if value == "" {
		return defaultValue
	}

	return value
}

func readEnvInt(shortkey string, defaultValue int) int {
	value := readEnv(shortkey, "")
	if value == "" {
		return defaultValue
	}

	valueInt, err := strconv.Atoi(value)
	if err != nil {
		panic(err)
	}

	return valueInt
}

func readEnvBool(shortkey string) bool {
	value := readEnv(shortkey, "")

	return (value == "1" || value == "true" || value == "yes")
}

func readEnvSlice(shortkey string) []string {
	return strings.Split(readEnv(shortkey, ""), " ")
}

// New config
func New() *Config {
	config := &Config{
		Frequency: readEnvInt("frequency", 10),
		Log: Log{
			Level: readEnv("log.level", "INFO"),
			IP:    readEnvBool("log.ip"),
		},
		Server: Server{
			Port: readEnv("server.port", "8080"),
		},
		Spam: Spam{
			Emails: readEnvSlice("spam.emails"),
			Hosts:  readEnvSlice("spam.hosts"),
		},
		Matrix: Matrix{
			Homeserver: readEnv("matrix.homeserver", ""),
			User: MatrixUser{
				Login:       readEnv("matrix.user.login", ""),
				Password:    readEnv("matrix.user.password", ""),
				DisplayName: readEnv("matrix.user.displayname", "Mother Miounne"),
			},
			Registration: MatrixRegistration{
				URL:    readEnv("matrix.registration.url", ""),
				Secret: readEnv("matrix.registration.secret", ""),
				RoomID: id.RoomID(readEnv("matrix.registration.room", "")),
				Notify: MatrixRegistrationNotify{
					New: readEnvBool("matrix.registration.notify.new"),
					Use: readEnvBool("matrix.registration.notify.use"),
				},
			},
		},
		Forms: readForms(),
		BMC: BMC{
			Token:  readEnv("bmc.token", ""),
			RoomID: id.RoomID(readEnv("bmc.room", "")),
			Notify: BMCnotify{
				Extras:     readEnvBool("bmc.notify.extras"),
				Members:    readEnvBool("bmc.notify.members"),
				Supporters: readEnvBool("bmc.notify.supporters"),
			},
		},
	}
	config.BMC.Enabled = isBMCEnabled(&config.BMC)
	config.Matrix.Enabled = isMatrixEnabled(&config.Matrix)
	config.Matrix.Registration.Enabled = isMatrixRegistrationEnabled(&config.Matrix.Registration)

	return config
}

// readForms from env
func readForms() []*Form {
	forms := []*Form{}
	formsSlice := readEnvSlice("forms")
	for _, name := range formsSlice {
		prefix := fmt.Sprintf("forms.%s.", name)
		fields := readEnvSlice(prefix + "fields")
		form := &Form{
			URL:        readEnv(prefix+"url", ""),
			RoomID:     id.RoomID(readEnv(prefix+"room", "")),
			Img:        readEnv(prefix+"img", "https://gitlab.com/uploads/-/system/project/avatar/27517262/miounne_avatar.png?width=212"),
			Name:       name,
			Fields:     fields,
			Redirect:   readEnv(prefix+"redirect", ""),
			Honeypot:   readEnv(prefix+"honeypot", ""),
			Ratelimit:  readEnv(prefix+"ratelimit", ""),
			Extensions: readEnvSlice(prefix + "extensions"),
		}
		forms = append(forms, form)
	}

	return forms
}

func isBMCEnabled(config *BMC) bool {
	return config.Token != ""
}

func isMatrixEnabled(config *Matrix) bool {
	return config.Homeserver != "" &&
		config.User.Login != "" &&
		config.User.Password != ""
}

func isMatrixRegistrationEnabled(config *MatrixRegistration) bool {
	return config.URL != "" && config.Secret != "" && config.RoomID != ""
}
