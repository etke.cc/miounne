package configuration

import "maunium.net/go/mautrix/id"

// Config struct
type Config struct {
	Frequency int
	Server    Server
	Matrix    Matrix
	BMC       BMC
	Forms     []*Form
	Spam      Spam
	Log       Log
}

// Log config
type Log struct {
	Level string
	IP    bool
}

// Server HTTP config
type Server struct {
	Port string
}

// Spam blocklist
type Spam struct {
	Emails []string
	Hosts  []string
}

// Matrix config
type Matrix struct {
	Enabled      bool
	Homeserver   string
	User         MatrixUser
	Registration MatrixRegistration
}

// MatrixUser config
type MatrixUser struct {
	Login       string
	Password    string
	DisplayName string
}

// MatrixRegistration integration
type MatrixRegistration struct {
	Enabled bool
	URL     string
	Secret  string
	RoomID  id.RoomID
	Notify  MatrixRegistrationNotify
}

// MatrixRegistrationNotify configuration
type MatrixRegistrationNotify struct {
	New bool
	Use bool
}

// Form config
type Form struct {
	URL        string
	Img        string
	Name       string
	RoomID     id.RoomID
	Fields     []string
	Honeypot   string
	Redirect   string
	Ratelimit  string
	Extensions []string
}

// BMC is a buymeacoffee config
type BMC struct {
	Enabled bool
	Token   string
	Notify  BMCnotify
	RoomID  id.RoomID
}

// BMCnotify is a buymeacoffee notifications config
type BMCnotify struct {
	Members    bool
	Extras     bool
	Supporters bool
}
