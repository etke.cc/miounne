package configuration

import (
	"os"
	"testing"

	"github.com/stretchr/testify/suite"
	"maunium.net/go/mautrix/id"
)

type ConfigurationSuite struct {
	suite.Suite
}

var values = map[string]string{
	"MIOUNNE_FREQUENCY":   "5",
	"MIOUNNE_LOG_LEVEL":   "TRACE",
	"MIOUNNE_LOG_IP":      "1",
	"MIOUNNE_SERVER_PORT": "12345",

	"MIOUNNE_MATRIX_HOMESERVER":       "https://example.com",
	"MIOUNNE_MATRIX_USER_LOGIN":       "@test:example.com",
	"MIOUNNE_MATRIX_USER_PASSWORD":    "password",
	"MIOUNNE_MATRIX_USER_DISPLAYNAME": "Test",

	"MIOUNNE_MATRIX_REGISTRATION_URL":    "https://example.org",
	"MIOUNNE_MATRIX_REGISTRATION_ROOM":   "!test:example.com",
	"MIOUNNE_MATRIX_REGISTRATION_SECRET": "secret",

	"MIOUNNE_SPAM_EMAILS": "ima@spammer.com definetelynotspam@gmail.com",
	"MIOUNNE_SPAM_HOSTS":  "spamer.com unitedspammers.org",

	"MIOUNNE_BMC_TOKEN": "test",
	"MIOUNNE_BMC_ROOM":  "!testbmc:example.com",

	"MIOUNNE_FORMS": "test1 test2",

	"MIOUNNE_FORMS_TEST1_URL":       "https://example.com/webhook",
	"MIOUNNE_FORMS_TEST1_IMG":       "https://example.com/logo.png",
	"MIOUNNE_FORMS_TEST1_FIELDS":    "name email notes",
	"MIOUNNE_FORMS_TEST1_REDIRECT":  "https://example.org",
	"MIOUNNE_FORMS_TEST1_RATELIMIT": "1r/s",
	"MIOUNNE_FORMS_TEST1_HONEYPOT":  "are-you-a-human",

	"MIOUNNE_FORMS_TEST2_ROOM":      "!test2@example.com",
	"MIOUNNE_FORMS_TEST2_FIELDS":    "surname email phone",
	"MIOUNNE_FORMS_TEST2_REDIRECT":  "https://example.com",
	"MIOUNNE_FORMS_TEST2_RATELIMIT": "5r/m",
	"MIOUNNE_FORMS_TEST2_HONEYPOT":  "arent-you-a-bot",
}

func (suite *ConfigurationSuite) SetupTest() {
	suite.T().Helper()
	for key, value := range values {
		os.Setenv(key, value)
	}
}

func (suite *ConfigurationSuite) TearDownTest() {
	suite.T().Helper()
	for key := range values {
		os.Unsetenv(key)
	}
}

func (suite *ConfigurationSuite) TestNew_DefaultValue() {
	os.Setenv("MIOUNNE_LOG_IP", "")
	config := New()

	suite.Equal(false, config.Log.IP)
}

func (suite *ConfigurationSuite) TestNew() {
	config := New()
	form1 := config.Forms[0]
	form2 := config.Forms[1]

	suite.Equal(5, config.Frequency)
	suite.Equal("TRACE", config.Log.Level)
	suite.Equal(true, config.Log.IP)
	suite.Equal("12345", config.Server.Port)
	suite.True(config.Matrix.Enabled)
	suite.Equal("https://example.com", config.Matrix.Homeserver)
	suite.Equal("@test:example.com", config.Matrix.User.Login)
	suite.Equal("password", config.Matrix.User.Password)
	suite.Equal("Test", config.Matrix.User.DisplayName)
	suite.True(config.Matrix.Registration.Enabled)
	suite.Equal("https://example.org", config.Matrix.Registration.URL)
	suite.Equal(id.RoomID("!test:example.com"), config.Matrix.Registration.RoomID)
	suite.Equal("secret", config.Matrix.Registration.Secret)
	suite.ElementsMatch([]string{"ima@spammer.com", "definetelynotspam@gmail.com"}, config.Spam.Emails)
	suite.ElementsMatch([]string{"spamer.com", "unitedspammers.org"}, config.Spam.Hosts)
	suite.Equal("test1", form1.Name)
	suite.Equal("https://example.com/webhook", form1.URL)
	suite.Equal("https://example.com/logo.png", form1.Img)
	suite.ElementsMatch([]string{"name", "email", "notes"}, form1.Fields)
	suite.Equal("https://example.org", form1.Redirect)
	suite.Equal("1r/s", form1.Ratelimit)
	suite.Equal("are-you-a-human", form1.Honeypot)
	suite.Empty(form1.RoomID)
	suite.Equal("test2", form2.Name)
	suite.Equal(id.RoomID("!test2@example.com"), form2.RoomID)
	suite.ElementsMatch([]string{"surname", "email", "phone"}, form2.Fields)
	suite.Equal("https://example.com", form2.Redirect)
	suite.Equal("arent-you-a-bot", form2.Honeypot)
	suite.Equal("5r/m", form2.Ratelimit)
	suite.Equal("https://gitlab.com/uploads/-/system/project/avatar/27517262/miounne_avatar.png?width=212", form2.Img)
	suite.Empty(form2.URL)
	suite.Equal("test", config.BMC.Token)
	suite.Equal(id.RoomID("!testbmc:example.com"), config.BMC.RoomID)
}

func TestConfigurationSuite(t *testing.T) {
	suite.Run(t, new(ConfigurationSuite))
}
