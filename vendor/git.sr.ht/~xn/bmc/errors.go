package bmc

import "fmt"

// Error is a base representation of any Buymeacoffee API error
type Error struct {
	Code   int    `json:"error_code"`
	Reason string `json:"reason"`
}

// Error returns human-readable error info
func (e *Error) Error() string {
	return fmt.Sprintf("%s (error code: %d)", e.Reason, e.Code)
}
