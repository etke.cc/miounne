package bmc

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

// API url
const API = "https://developers.buymeacoffee.com/api/v1/"

// Client for Buymeacoffee API
type Client struct {
	authorization string
	API           string
	Client        *http.Client
}

type errorResponse struct {
	Error
}

// NewClient for Buymeacoffee API
func NewClient(token string) *Client {
	return &Client{
		API:           API,
		Client:        newHTTPClient(),
		authorization: "Bearer " + token,
	}
}

// newHTTPClient returns an http client with tuned settings
func newHTTPClient() *http.Client {
	transport, ok := http.DefaultTransport.(*http.Transport)
	if ok {
		transport.MaxIdleConns = 100
		transport.MaxIdleConnsPerHost = 100
	}

	return &http.Client{
		Timeout:   10 * time.Second,
		Transport: transport,
	}
}

func (c *Client) parseError(code int, body []byte) *Error {
	if code == http.StatusOK {
		return nil
	}

	response := errorResponse{}
	err := json.Unmarshal(body, &response)
	if err == nil && response.Code != 0 && response.Reason != "" {
		return &response.Error
	}

	return nil
}

// Send an http request to buymeacoffee API
func (c *Client) Send(method string, endpoint string, body []byte, v interface{}) error {
	ctx := context.Background()
	req, reqErr := http.NewRequestWithContext(ctx, method, c.API+endpoint, bytes.NewBuffer(body))
	if reqErr != nil {
		return &Error{Reason: fmt.Sprintf("cannot create request: %v", reqErr)}
	}
	req.Header.Add("Authorization", c.authorization)
	resp, respErr := c.Client.Do(req)
	if respErr != nil {
		return &Error{Reason: fmt.Sprintf("cannot send request: %v", respErr)}
	}
	body, bodyErr := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if bodyErr != nil {
		return &Error{Reason: fmt.Sprintf("cannot read body: %v", bodyErr)}
	}

	parsedErr := c.parseError(resp.StatusCode, body)
	if parsedErr != nil {
		return parsedErr
	}

	if v == nil {
		return nil
	}

	unmarshalErr := json.Unmarshal(body, &v)
	if unmarshalErr != nil {
		return &Error{Reason: fmt.Sprintf("cannot unmarshal body: %v", unmarshalErr)}
	}

	return nil
}
