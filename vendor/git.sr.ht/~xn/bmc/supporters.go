package bmc

import "fmt"

// Supporter is BMC onetime supporter
type Supporter struct {
	Country            string `json:"country"`
	IsRefunded         int    `json:"is_refunded"`
	PayerEmail         string `json:"payer_email"`
	PayerName          string `json:"payer_name"`
	PaymentPlatform    string `json:"payment_platform"`
	Refererr           string `json:"referer"`
	SupportCoffeePrice string `json:"support_coffee_price"`
	SupportCoffees     int    `json:"support_coffees"`
	SupportCreatedOn   string `json:"support_created_on"`
	SupportCurrency    string `json:"support_currency"`
	SupportEmail       string `json:"support_email"`
	SupportID          int    `json:"support_id"`
	SupportNote        string `json:"support_note"`
	SupportNotePinned  int    `json:"support_note_pinned"`
	SupportUpdatedOn   string `json:"support_updated_on"`
	SupportVisibility  int    `json:"support_visibility"`
	SupporterName      string `json:"supporter_name"`
	TransactionID      string `json:"transaction_id"`
	TransferID         string `json:"transfer_id"`
}

// Supporters is a slice of BMC supporters
type Supporters []*Supporter

type supportersResponse struct {
	Total    int        `json:"total"`
	Page     int        `json:"current_page"`
	LastPage int        `json:"last_page"`
	Data     Supporters `json:"data"`
}

// GetSupporter returns a BMC supporter by support ID
func (c *Client) GetSupporter(supportID int) (*Supporter, error) {
	var response Supporter
	endpoint := fmt.Sprintf("supporters/%d", supportID)
	err := c.Send("GET", endpoint, nil, &response)

	return &response, err
}

// GetSupporters returns a slice of BMC supporters
func (c *Client) GetSupporters() (Supporters, error) {
	var response supportersResponse
	endpoint := "supporters"

	err := c.Send("GET", endpoint, nil, &response)
	if err != nil {
		return nil, err
	}
	supporters := make(Supporters, 0, response.Total)
	supporters = append(supporters, response.Data...)

	for response.Page < response.LastPage {
		endpoint = fmt.Sprintf("supporters?page=%d", response.Page+1)
		response = supportersResponse{}
		err = c.Send("GET", endpoint, nil, &response)
		if err != nil {
			return nil, err
		}
		supporters = append(supporters, response.Data...)
	}

	return supporters, nil
}
