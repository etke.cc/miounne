package models

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type ModelSuite struct {
	suite.Suite
}

func (suite *ModelSuite) SetupTest() {
	suite.T().Helper()
}

func TestModelSuite(t *testing.T) {
	suite.Run(t, new(ModelSuite))
}
